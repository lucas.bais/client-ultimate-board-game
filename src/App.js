import React from 'react';
import {subscribeToErrors} from './api';
import {useToast} from "@chakra-ui/react";

import {HashRouter, Route, Switch} from "react-router-dom";
import LoginForm from "./auth/login/LoginForm";
import RegisterForm from "./auth/register/RegisterForm";
import Landing from "./landing/Landing";
import RoomsLobby from "./rooms/RoomsLobby";
import Room from "./rooms/Room";

function App() {
    const toast = useToast();
    const [error, onErrorMessageReceived] = React.useState(null);

    React.useEffect(() => {
        subscribeToErrors(onErrorMessageReceived)
    }, []);

    if (error) {
        toast({
            title: error.errorMessage,
            status: "error",
            duration: 9000,
            isClosable: true,
        });
    }

    return (
        <div>
            <HashRouter>
                <Switch>
                    <Route exact path="/register" component={RegisterForm}/>
                    <Route exact path="/login" component={LoginForm}/>
                    <Route exact path="/rooms" component={RoomsLobby}/>
                    <Route path="/room/:id" component={Room}/>
                    <Route path="/" component={Landing}/>
                </Switch>
            </HashRouter>
        </div>
    );
}

export default App;
