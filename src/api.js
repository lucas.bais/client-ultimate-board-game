import openSocket from 'socket.io-client';
import userStorage from "./auth/source/local/UserStorage";

const socketUrl = process.env.REACT_APP_REMOTE_SOCKET_URL;

const socket = openSocket(socketUrl);

const trace = fn => (...args) => console.log(fn.name, args) || fn(...args);

function subscribeToGameState(onGameStateReceived) {
    socket.on('gameState', trace(onGameStateReceived));
}

function startGame(roomId) {
    socket.emit('startGame', {roomId: roomId, accessToken: userStorage.recoverUser().accessToken});
}

function joinRoom(id, password) {
    const user = userStorage.recoverUser()
    socket.emit('joinRoom', {id, password, userId: user.userId, accessToken: user.accessToken});
}

function subscribeToErrors(onErrorReceived) {
    socket.on('error', trace(onErrorReceived))
}

function subscribeToRooms(onRoomsReceived) {
    socket.on('rooms', trace(onRoomsReceived));
}

function subscribeToRoom(onRoomReceived) {
    socket.on('room', trace(onRoomReceived));
}

const ActionTypes = {
    Flip: 'flip',
    Shuffle: 'shuffle',
    Take: 'take',
    Move: 'move_object'
}

function action(actionType, gameId, payload) {
    socket.emit('action', {
        action: actionType,
        gameId: gameId,
        payload: payload,
        accessToken: userStorage.recoverUser().accessToken,
    });
}

export {
    subscribeToErrors,
    subscribeToRooms,
    subscribeToGameState,
    subscribeToRoom,
    joinRoom,
    startGame,
    action,
    ActionTypes,
};
