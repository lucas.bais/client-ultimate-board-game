import React from 'react';
import {withRouter} from "react-router-dom";
import {Button} from "@chakra-ui/react"
import userStorage from "./source/local/UserStorage";

const LogoutButton = ({history}) => {

    const clearUserAndRedirect = () => {
        userStorage.clear();
        history.push('/');
    };

    return (
        <Button colorScheme="teal" variant="ghost" onClick={clearUserAndRedirect}>Cerrar sesión</Button>
    )
};

export default withRouter(LogoutButton);