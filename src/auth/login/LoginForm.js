import React from 'react';
import useInput from "../../hooks/useInput";
import {
    VStack, Button, Box, Center,
    Heading, FormLabel, FormControl,
    FormErrorMessage, Link as UILink
} from "@chakra-ui/react"

import {Link, Redirect} from "react-router-dom";
import {userService} from "../source/remote/UserService";
import userStorage from "../source/local/UserStorage";

export default () => {

    const [emailValue, emailInput] = useInput({placeholder: '', name: 'loginEmail'});
    const [passwordValue, passwordInput] = useInput({placeholder: '', type: 'password', name: 'loginPass'});

    const [state, onState] = React.useState({
        isLoading: false,
        signInCompleted: false,
        errorMessage: null
    });

    const handleSignIn = () => {
        onState({
            isLoading: true,
            errorMessage: null,
            signInCompleted: false,
        })

        userService.login({
            email: emailValue,
            password: passwordValue
        }, (userCredentials) => {
            userStorage.saveUser(userCredentials)
            onState({
                isLoading: false,
                signInCompleted: true,
                errorMessage: null,
            })
        }, (errorMessage) => {
            onState({
                isLoading: false,
                signInCompleted: false,
                errorMessage: errorMessage
            })
        });
    }

    if (state.signInCompleted) return (<Redirect to={`/rooms`}/>);

    return (
        <Center>
            <Box maxW="sm" borderRadius="lg" overflow="hidden" boxShadow="base" p={8} mt={16}>
                <VStack spacing={4}>
                    <Heading>Ingresá</Heading>

                    <FormControl isInvalid={state.errorMessage}>
                        <FormLabel>Email</FormLabel>
                        {emailInput}
                        <FormLabel>Contraseña</FormLabel>
                        {passwordInput}
                        <FormErrorMessage>{state.errorMessage}</FormErrorMessage>
                    </FormControl>

                    <UILink color="teal.500">
                        <Link to={"/register"}> ¿Todavía no tenés cuenta? ¡Registrate!</Link>
                    </UILink>

                    <Button colorScheme="teal" variant="solid" onClick={handleSignIn}
                            isLoading={state.isLoading}>Ingresar</Button>
                </VStack>
            </Box>
        </Center>
    );
}