import React from 'react';
import useInput from "../../hooks/useInput";
import {
    Box,
    Button,
    Center,
    FormControl,
    FormErrorMessage,
    Heading,
    VStack,
    FormLabel,
    Link as UILink
} from "@chakra-ui/react";
import {Link, Redirect} from "react-router-dom";
import {userService} from "../source/remote/UserService";
import userStorage from "../source/local/UserStorage";

export default () => {

    const [emailValue, emailInput] = useInput({placeholder: '', name: 'registerEmail'});
    const [passwordValue, passwordInput] = useInput({placeholder: '', type: 'password', name: 'passwordEmail'});

    const [state, onState] = React.useState({
        isLoading: false,
        errorMessage: null,
        signUpCompleted: false
    });

    const handleSignUp = () => {
        onState({
            isLoading: true,
            errorMessage: null,
            signUpCompleted: false
        })

        userService.register({
            email: emailValue,
            password: passwordValue
        }, (userCredentials) => {
            userStorage.saveUser(userCredentials)
            onState({
                isLoading: false,
                signUpCompleted: true,
                errorMessage: null
            })
        }, (errorMessage) => {
            onState({
                isLoading: false,
                signUpCompleted: false,
                errorMessage: errorMessage
            })
        });
    }

    if(state.signUpCompleted) return (<Redirect to={`/rooms`}/>);

    return (
        <Center>
            <Box maxW="sm" borderRadius="lg" overflow="hidden" boxShadow="base" p={8} mt={16}>
                <VStack spacing={4}>
                    <Heading>Registra tu usuario</Heading>

                    <FormControl isInvalid={state.errorMessage}>
                        <FormLabel>Email</FormLabel>
                        {emailInput}
                        <FormLabel>Contraseña</FormLabel>
                        {passwordInput}
                        <FormErrorMessage>{state.errorMessage}</FormErrorMessage>
                    </FormControl>

                    <UILink color="teal.500"><Link to={"/login"}> ¿Ya tenes cuenta? ¡Ingresá!</Link></UILink>

                    <Button colorScheme="teal" variant="solid" onClick={handleSignUp}
                            isLoading={state.isLoading}>Registrar</Button>
                </VStack>
            </Box>
        </Center>
    );
}