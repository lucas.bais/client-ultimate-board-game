const UGX_USER = 'UGX_USER_v1';

const userStorage = {
    recoverUser,
    saveUser,
    clear,
    hasUser
}

function saveUser(user) {
    localStorage.setItem(UGX_USER, JSON.stringify(user));
}

function hasUser() {
    return recoverUser() != null
}

function clear() {
    localStorage.removeItem(UGX_USER);
}

function recoverUser() {
    return JSON.parse(localStorage.getItem(UGX_USER))
}

export default userStorage