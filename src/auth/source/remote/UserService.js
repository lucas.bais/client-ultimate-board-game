import axios from 'axios'

function UserService(httpClient) {

    const authUrl = process.env.REACT_APP_REMOTE_URL + "auth";

    this.register = ({email, password}, successCallback, errorCallback) => {
        httpClient.post(`${authUrl}/sign_up`, {
            email: email,
            password: password
        }).then(function (response) {
            successCallback(response.data)
        }).catch(function (err) {
            if(err.response == null) {
                errorCallback("Something went wrong when calling endpoint /sign_up")
                return
            }
            errorCallback(err.response.data.errorMessage)
        });
    };

    this.login = ({email, password}, successCallback, errorCallback) => {
        httpClient.post(`${authUrl}/sign_in`, {
            email: email,
            password: password
        }).then(function (response) {
            successCallback(response.data)
        }).catch(function (err) {
            if(err.response == null) {
                errorCallback("Something went wrong when calling endpoint /sign_in")
                return
            }
            errorCallback(err.response.data.errorMessage)
        });
    };
}


const userService = new UserService(axios)

export {
    userService,
}