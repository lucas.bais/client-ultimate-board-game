import React from "react";

export default ({text}) => {
    return (text? <div><h1>{text}</h1></div> : <div><h1>Cargando...</h1></div>);
}