import React, { memo, useRef } from "react";
import { useDrag, useDrop } from "react-dnd";

const ItemTypes = {
    GAME_OBJECT: 'GAME_OBJECT',
}

const DragItem = memo(({id, onMoveItem, children}) => {

    const ref = useRef(null);

    const [{ isDragging }, connectDrag] = useDrag({

        item: { id, type: ItemTypes.GAME_OBJECT },

        collect: monitor => {
            return {
                isDragging: monitor.isDragging()
            };
        }
    });

    const [, connectDrop] = useDrop({
        accept: ItemTypes.GAME_OBJECT,
        drop: (item) => {
            onMoveItem(item.id, id);
        }
    });

    connectDrag(ref);
    // connectDrag(refDrag); // TOFIX
    connectDrop(ref);

    const opacity = isDragging ? 0.5 : 1;
    const containerStyle = { opacity };

    return React.Children.map(children, child =>
        React.cloneElement(child, {
            forwardedRef: ref,
            style: containerStyle,
            isDragging,
        })
    );
});

export default DragItem;