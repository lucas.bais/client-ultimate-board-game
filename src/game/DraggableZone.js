import React from 'react';
import styled from 'styled-components';
import {action, ActionTypes} from '../api';
import {Badge, Flex, Image} from '@chakra-ui/react';

const BaseZone = styled.div`
    width: 10rem;
    height: 10rem;
    border: 1px dashed gray;
`;
const ContentZone = styled(BaseZone)`
    display: flex;
    align-items: flex-start;
    .stacker {
      width: 9rem;
      height: 8rem;
      position: relative;
      left: 1rem;
      top: 1rem;
    }
    .stacker img {
      height: 80%;
      position: absolute;
    }
    div {
      cursor: pointer;
    }
`;

const EmptyZone = styled(BaseZone)``;

const calcTranslate = index => {
    return `translate(${index}rem, ${index}rem)`;
}
const last = arr => arr[arr.length - 1];
const ZoneContainer = ({zone, gameId, onZoom, forwardedRefDrag, isDragging}) => {
    if (!zone.objects.length) return <EmptyZone/>;
    const upToTopThree = zone.objects.slice(zone.objects.length - Math.min(zone.objects.length, 3));
    const topCard = last(upToTopThree);
    return <ContentZone>
        <Flex direction="column">
            {onZoom && <span role='img' aria-label='zoom' onClick={() => onZoom(topCard)}>🔎</span>}
            {zone.objects.length > 3 &&
            <Badge colorScheme="purple">{zone.objects.length}</Badge>}
            {zone.objects.length > 1 &&
            <span role='img' aria-label='shuffle'
                  onClick={() => action(ActionTypes.Shuffle, gameId, {zoneId: zone.id})}>🔀</span>}
            {zone.objects.length >= 1 &&
            <span role='img' aria-label='take'
                  onClick={() => action(ActionTypes.Take, gameId, {zoneId: zone.id, quantity: 1})}>👇</span>}
        </Flex>
        <div className="stacker">
            {upToTopThree.map((img, index) => {
                const isTopCard = img === topCard;
                const onDoubleClick = () => action(ActionTypes.Flip, gameId, {objectId: img.id});
                return <Image
                    key={`extra-card-${index}`}
                    alt='asset'
                    src={img.image}
                    transform={calcTranslate(index)}
                    onDoubleClick={isTopCard ? onDoubleClick : Function.prototype}
                    ref={isTopCard ? forwardedRefDrag : null}
                />;
            })}
        </div>

    </ContentZone>;
};

const ZoneWrapper = styled.div`
`;

const DraggableZone = ({forwardedRef, renderContent, ...props}) => (
    <ZoneWrapper ref={forwardedRef}>
        {renderContent(props)}
    </ZoneWrapper>
);

export {
    DraggableZone,
    ZoneContainer
};