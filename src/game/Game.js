import React from 'react';
import DragItem from './DragItem';
import {DraggableZone, ZoneContainer} from './DraggableZone';
import {action, ActionTypes} from '../api';
import {
    Box,
    Flex,
    Heading,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalOverlay,
    Select, Image,
    useDisclosure
} from '@chakra-ui/react';

import userStorage from '../auth/source/local/UserStorage';

const moveZone = (gameId, zones) => (sourceId, destinationId) => {

    const sourceIndex = zones.findIndex(
        zone => zone.id === sourceId
    );

    const destinationIndex = zones.findIndex(
        zone => zone.id === destinationId
    );

    // If source/destination is unknown, do nothing.
    if (sourceIndex === -1 || destinationIndex === -1) {
        return;
    }

    // Remote
    const zoneSource = zones[sourceIndex];
    const zoneDestination = zones[destinationIndex];
    const {objects} = zoneSource;
    if (objects.length) {
        action(ActionTypes.Move, gameId, {
            zoneId: zoneSource.id,
            destinationZoneId: zoneDestination.id,
            targetObjectId: objects[objects.length - 1].id,
        });
    }
};

const ZoomModal = ({objectToZoom, isOpen, onClose}) => {
    return <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay onClick={onClose}/>
        <ModalContent>
            {onClose && <ModalCloseButton/>}
            <ModalBody>
                {objectToZoom && <Image src={objectToZoom.image} width="30vw"/>}
            </ModalBody>
        </ModalContent>
    </Modal>;
};

const ElasticBoard = ({zones, ...rest}) => {
    const zonesWithObjects = zones.filter(z => z.objects.length);
    const lastZoneWithElementIndex = zones.indexOf(zonesWithObjects[zonesWithObjects.length - 1])
    const zonesToRender = zones.slice(0, lastZoneWithElementIndex + 2);
    return (<Board zones={zonesToRender} {...rest} />);
}

const Board = ({zones, handleMoveItem, gameId, title, onZoom, containerStyles = {}}) => (<Box {...containerStyles}>
    {title && <Heading size="sm" mb="0.5rem">{title}</Heading>}
    <Flex justifyContent="start" wrap="wrap">
        {zones.map(zone => (
            <DragItem key={zone.id} id={zone.id} onMoveItem={handleMoveItem}>
                <DraggableZone
                    renderContent={props => <ZoneContainer zone={zone} gameId={gameId}
                                                           onZoom={onZoom} {...props} />}/>
            </DragItem>
        ))}
    </Flex>
</Box>);

const handContainerStyles = {
    borderBottom: '0.25rem solid rgba(150, 150, 0, 0.3)',
    background: 'rgba(255, 255, 0, 0.1)',
    padding: '1rem',
}
const tableContainerStyles = {
    background: 'rgba(0, 200, 150, 0.1)',
    borderBottom: '0.25rem solid rgba(50, 100, 0, 0.3)',
    padding: '1rem',
}
const assetsContainerStyles = {
    borderRight: '0.25rem solid rgba(50, 100, 50, 0.3)',
    background: 'rgba(50, 200, 50, 0.1)',
    padding: '1rem',
    maxWidth: '15rem',
}

const smartIdCleaner = assetId => assetId
    .replace('asset_deck_', '')
    .replace('cards_', '')
    .replace('_cards', '')

export default ({gameId, gameState}) => {
    const [objectToZoom, setObjectToZoom] = React.useState(null);
    const {isOpen, onOpen, onClose} = useDisclosure();
    const [selectedAsset, setSelectedAsset] = React.useState(gameState.assets ? gameState.assets[0].id : null);

    const handleModalClose = () => {
        setObjectToZoom(null);
        onClose();
    };

    if (!isOpen && objectToZoom) onOpen();

    const currentPlayer = gameState.players.find(player => (userStorage.recoverUser().userId === player.id));
    const {hand} = currentPlayer;
    const handleMoveItem = moveZone(gameId, gameState.assets.concat(hand).concat(gameState.board));
    const assetToShow = [gameState.assets.find(asset => asset.id === selectedAsset) || gameState.assets[0]];
    const boardCommon = {
        handleMoveItem,
        gameId,
        onZoom: setObjectToZoom,
    };

    return (
        <Flex>
            <Box {...assetsContainerStyles}>
                <Select placeholder="Selecciona mazo" size="sm" onChange={e => setSelectedAsset(e.target.value)}
                        mb="1rem">
                    {gameState.assets.map(asset => <option
                        key={asset.id}
                        value={asset.id}
                        selected={selectedAsset === asset.id}>
                        {asset.name || smartIdCleaner(asset.id)}
                    </option>)}
                </Select>
                <Board
                    zones={assetToShow}
                    {...boardCommon}
                />
            </Box>
            <Box>
                <Board
                    title="Mesa"
                    zones={gameState.board}
                    containerStyles={tableContainerStyles}
                    {...boardCommon}
                />
                <ElasticBoard
                    title="Mano"
                    zones={hand}
                    containerStyles={handContainerStyles}
                    {...boardCommon}
                />
            </Box>
            <ZoomModal isOpen={isOpen} onClose={handleModalClose} objectToZoom={objectToZoom}/>
        </Flex>
    );
}