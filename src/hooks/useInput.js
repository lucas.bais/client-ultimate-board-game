import React, { useState } from 'react';
import { Input } from "@chakra-ui/react";

export default function useInput(inputProps) {
  const [value, setValue] = useState("");
  const props = {
    ...inputProps,
    value,
    onChange: e => setValue(e.target.value),
  };
  const input = <Input {...props} />;
  return [value, input];
}