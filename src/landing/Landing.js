import React from "react";
import userStorage from "../auth/source/local/UserStorage";
import {Redirect} from "react-router-dom";
import LoginForm from "../auth/login/LoginForm";

export default () => {
    const [user] = React.useState(userStorage.hasUser());

    if (user) {
        return (<Redirect to={`/rooms`}/>)
    } else {
        return (
            <div>
                <LoginForm/>
            </div>
        );
    }


}