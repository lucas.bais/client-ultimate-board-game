import React from 'react';
import {Flex, Heading, Button} from '@chakra-ui/react';
import {Redirect} from "react-router-dom";
import useInput from '../hooks/useInput';
import {roomService} from "./source/remote/RoomService";

export default () => {
    const [roomName, roomNameInput] = useInput({placeholder: 'Nombre de la sala', mb: '0.5em'});
    const [roomPassword, roomPasswordInput] = useInput({placeholder: 'Contraseña (opcional)', mb: '0.5em'});
    const [state, onState] = React.useState({
        isLoading: false,
        room: null,
        error: null
    });

    const createRoomHandler = () => {
        onState({isLoading: true, room: null, error: null});

        roomService.newRoom({name: roomName, password: roomPassword}, (room) => {
                onState({
                    isLoading: false,
                    room: room,
                    error: null
                })
            },
            (error) => {
                // TODO error
                onState({
                    isLoading: false,
                    room: null,
                    error: error
                })
            });
    }

    const createRoomForm = () => (
        <Flex align="flex-start" direction="column" borderWidth="1px" borderRadius="lg" overflow="hidden" p="6">
            <Heading size="md" mb="1rem">Crear sala</Heading>
            {roomNameInput}
            {roomPasswordInput}
            <Button colorScheme="teal" variant="solid" onClick={createRoomHandler}
                    isLoading={state.isLoading}>Crear sala</Button>
        </Flex>
    );

    return (state.room ? <Redirect push to={`/room/${state.room.id}`}/> : createRoomForm());
}