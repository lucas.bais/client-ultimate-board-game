import React from "react";
import Loading from "../common/Loading";
import {subscribeToRoom, subscribeToGameState, startGame, joinRoom} from '../api';
import Game from "../game/Game";
import RoomLobby from "./RoomWaitingRoom";
import {Button, Flex} from '@chakra-ui/react';
import {useParams, useLocation} from "react-router-dom";

export default () => {
    let {id} = useParams();
    let {search} = useLocation();

    const query = new URLSearchParams(search);
    const password = query.get('pass');

    const [roomState, onRoomState] = React.useState({room: null, hasJoined: false});
    const [gameState, onGameStateReceived] = React.useState(null);

    React.useEffect(() => {
        subscribeToRoom((room) => {
            console.log(room)
            onRoomState({
                hasJoined: true,
                room: room,
            })
        });

        subscribeToGameState(onGameStateReceived);
    }, [])

    // This will make the user join the room.
    if (!roomState.room) {
        joinRoom(id, password)
    }

    function startGameButton() {
        return ((roomState.room) ? <Flex maxW={320 * 2} ml="auto" mr="auto" direction="column" align="flex-start">
            <Button colorScheme="teal" variant="solid" onClick={() => startGame(roomState.room.id)}>Empezar el
                juego</Button>
        </Flex> : <Loading text={"Cargando sala..."}/>);
    }

    return (<div>
        {!(roomState.room) ? <Loading text={"Cargando estado de juego..."}/> : <RoomLobby room={roomState.room}/>}
        {gameState ? <Game gameState={gameState} gameId={roomState.room.id}/> : startGameButton()}
    </div>);
}