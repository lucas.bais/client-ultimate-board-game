import React, {useState} from 'react';
import {startGame} from '../api';
import {Heading, Button, Flex, Box, Spacer, Text, List, ListItem} from '@chakra-ui/react';

export default ({room}) => {
    const [isExpanded, setIsExpanded] = useState(false);
    const renderContent = () => (
        <React.Fragment>
            <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p="6" w="40%" m="6">
                <Heading>Estamos en la sala: {room.name}</Heading>
                <Text fontSize="xl">Cantidad de personas en la sala ({room.guests.length} 👨‍👨‍👧‍👧)</Text>
                <List>
                    {room.guests.length ? room.guests.map(guest => <ListItem key={guest.name}>{guest.name}</ListItem>) :
                        <ListItem>No personas en esta sala</ListItem>}
                </List>
                <Flex pt="4">
                    <Button colorScheme="green" variant="solid" onClick={() => setIsExpanded(!isExpanded)}>Ocultar
                        opciones</Button>
                    <Spacer/>
                    <Button colorScheme="red" variant="solid" onClick={() => startGame(room.id)}>Reiniciar
                        Juego</Button>
                </Flex>
            </Box>
        </React.Fragment>);
    return <div>
        {isExpanded && renderContent()}
        {!isExpanded &&
        <Button colorScheme="teal" variant="solid" onClick={() => setIsExpanded(!isExpanded)}>Opciones de juego
            <span role="img" aria-label="computer">💻</span></Button>}

    </div>;
};