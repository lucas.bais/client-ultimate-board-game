import React from 'react';
import {
    Box,
    Heading,
    Input,
    Button,
    ListItem,
    List,
    Text,
    SimpleGrid,
} from '@chakra-ui/react';
import {Link, Redirect} from "react-router-dom";
import {roomService} from "./source/remote/RoomService";
import {subscribeToRooms, joinRoom, subscribeToRoom} from '../api';

const PublicLink = ({room}) => (
    <Link to={`/room/${room.id}`}>
        <Button colorScheme="teal" variant="solid">Entrar</Button>
    </Link>
)

const PrivateLink = ({room}) => {
    const [roomPassword, setRoomPassword] = React.useState(null);
    const [redirect, setRedirect] = React.useState(false);

    React.useEffect(() => {
        subscribeToRoom(successRoom => {
            if (room.id !== successRoom.id) return null;
            setRedirect(true);
        });
    });

    if (redirect) return <Redirect to={`/room/${room.id}?pass=${roomPassword}`}/>

    return (
        <Box display="flex" flexDirection="row">
            <Input placeholder="password" onChange={event => setRoomPassword(event.target.value)}/>
            <Button colorScheme="teal" variant="solid" onClick={() => {
                joinRoom(room.id, roomPassword)
            }}>Entrar</Button>
        </Box>
    );
}

export default () => {

    const [state, onState] = React.useState({
        rooms: [],
        // Intended, if leave empty without error will request the rooms many times.
        requested: false,
        error: null
    });

    React.useEffect(() => {
        subscribeToRooms((rooms) => {
            onState({
                rooms: rooms,
                requested: true,
            })
        });
    }, []);

    if (!state.requested) {
        roomService.rooms((rooms) => {
            onState({
                rooms: rooms,
                requested: true
            })
        }, (err) => {
            onState({
                rooms: [],
                requested: true,
                error: err
            })
        })
    }

    const renderRoomItem = room => <ListItem key={room.id}>
        <SimpleGrid mt="1rem" columns={3} spacing={10}>
            <Text>{room.name}</Text>
            <Text>{room.guests.length < 10 ? `${room.guests.length}` : '>10'}</Text>
            {room.isPublic ? <PublicLink room={room}/> : <PrivateLink room={room}/>}
        </SimpleGrid>

    </ListItem>;

    return (
        state.rooms.length ?
            <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p="6">
                <Heading size="md" mb="1rem">Salas</Heading>
                <SimpleGrid size="md" columns={3} spacing={10}>
                    <Heading size="sm">Nombre</Heading>
                    <Heading size="sm">Cantidad de usuarios</Heading>
                    <Heading size="sm">Opciones</Heading>
                </SimpleGrid>
                <List>
                    {state.rooms.map(renderRoomItem)}
                </List></Box> : <Heading size="sm">No hay salas disponibles</Heading>
    );
}