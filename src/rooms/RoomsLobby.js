import React from 'react';
import {
    Stack,
    Center,
} from '@chakra-ui/react';
import CreateRoomForm from './CreateRoomForm';
import UserHeader from "../user/UserHeader";
import RoomsList from "./RoomsList";

export default () => {
    return (
        <Center>
            <Stack mt="1em">
                <UserHeader/>
                <Stack direction="row">
                    <CreateRoomForm/>
                    <RoomsList/>
                </Stack>
            </Stack>
        </Center>
    );
}