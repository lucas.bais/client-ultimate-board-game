import axios from 'axios'
import userStorage from "../../../auth/source/local/UserStorage";

function RoomService(httpClient) {

    const roomUrl = process.env.REACT_APP_REMOTE_URL + "rooms";

    this.rooms = (successCallback, errorCallback) => {
        httpClient.get(`${roomUrl}`)
            .then(function (response) {
                successCallback(response.data)
            }).catch(function (err) {
            if (err == null) {
                errorCallback("error body empty")
                return
            } else if (err.response == null) {
                errorCallback("error body response is null")
                return
            }
            errorCallback(err.response.data.errorMessage)
        });
    }

    this.newRoom = ({name, password}, successCallback, errorCallback) => {
        axios.post(`${roomUrl}`, {
            name: name,
            password: password
        }, {
            headers: {
                "Authorization": `Bearer ${userStorage.recoverUser().accessToken}`
            }
        })
            .then(function (response) {
                successCallback(response.data)
            })
            .catch(function (err) {
                if(err.response == null) {
                    errorCallback("Something went wrong when calling endpoint /sign_up")
                    return
                }
                errorCallback(err.response.data.errorMessage)
            });
    }
}

const roomService = new RoomService(axios)

export {
    roomService
}