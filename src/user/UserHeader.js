import {Badge, Center, Flex, Heading, Spacer} from "@chakra-ui/react";
import LogoutButton from "../auth/LogoutButton";
import React from "react";
import userStorage from "../auth/source/local/UserStorage";

export default () => {
    const user = userStorage.recoverUser();
    return (user ? <Flex>
        <Center>
            <Badge colorScheme="purple">Usuario número: {user.userId}</Badge>
        </Center>
        <Spacer/>
        <LogoutButton/>
    </Flex> : <Heading>Usuario invitado - no registrado</Heading>)
}